
<!DOCTYPE html>
<html>
<head>
  <title>Table</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="">
<style type="text/css">
   table {
  border-collapse: separate;
  border-spacing: 4px;

}
table,
th,
td {
  border: 1px solid #cecfd5;
}
th,
td {
  padding: 10px 15px;
}
</style>
</head>
<body>

<form class="form-horizontal" role="form" method="post" action="views.php">
 <div class="form-group">
    <div class="col-sm-10 col-sm-offset-3">
    
<table>

 <h2>Customer Information Table</h2>
  <thead>
     <tr>
        <th>First_Name</th>
        <th>Last_Name</th>
        <th>Email_ID</th>
        <th>Age</th>
        <th>Seip_ID</th>
     
    </tr>
  </thead>
  <tbody>
     
    <tr>
        <td>Salah Uddin</td>
           <td>Chowdhury</td>
              <td>salahuddin6896@yahoo.com</td>
                 <td>26</td>
                    <td>165894</td>
        
    </tr>
    <tr>
        <<td>Shaida</td>
           <td>Sultana</td>
              <td>shaidasr@yahoo.com</td>
                 <td>26</td>
                    <td>165895</td>
    </tr>
    <tr>
        <td>Najma</td>
           <td>Rahim</td>
              <td>najma@yahoo.com</td>
                 <td>26</td>
                    <td>165896</td>
    </tr>
  </tbody>
  <tfoot>
</table>
</tfoot>
</table>
</form>
</body>
</html>